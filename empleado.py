#!/usr/bin/env python3

class Empleado:
    """Un ejemplo de clase para Empleados"""
    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s

    def calcula_impuestos (self):
        return self.nomina*0.30

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre,
                tax=self.calcula_impuestos())
el = Empleado("Jaime", 10000)
print(el)
# Cuando te dice que dos clases son cmo herencia
class Jefe(Empleado):
    
    def __init__(self, n, s, extra=0):
        super().__init__(n, s)
        self.bonus = extra
    
    def calcula_impuestos (self):
        return super().calcula_impuestos() + self.bonus*0.30
    

    def __str__(self):
        return "El jefe {name} debe pagar {tax:.2f}".format(name=self.nombre,
                tax=self.calcula_impuestos())